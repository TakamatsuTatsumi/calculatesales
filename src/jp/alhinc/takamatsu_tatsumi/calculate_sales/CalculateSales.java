package jp.alhinc.takamatsu_tatsumi.calculate_sales;

import java.io.IOException;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		BranchFileReader branchFileReader = new BranchFileReader(args[0]);

		try {
			Map<String, String> branchMap = branchFileReader.readBranchFile();
			if(branchMap.isEmpty()) {
				return;
			}

			SalesReader salesReader = new SalesReader(args[0], branchMap.keySet());
			Map<String, Long> salesMap = salesReader.readSalesFile();
			if(salesMap.isEmpty()) {
				return;
			}

			BranchSalesWriter branchSalesWriter = new BranchSalesWriter(args[0], salesMap, branchMap);
			branchSalesWriter.outputBranchSales();
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		}
	}
}