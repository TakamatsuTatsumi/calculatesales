package jp.alhinc.takamatsu_tatsumi.calculate_sales;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class BranchSalesWriter {
	private String path;
	private Map<String, Long>salesMap;
	private Map<String, String> branchMap;

	BranchSalesWriter(String path, Map<String, Long>salesMap, Map<String, String> branchMap) {
		this.path = path;
		this.branchMap = branchMap;
		this.salesMap = salesMap;
	}

	//支店別集計ファイルの作成
	public void outputBranchSales() throws IOException {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File(path, "branch.out"), true));){
			for(String branchCode : branchMap.keySet()) {
				bw.write(branchCode + "," + branchMap.get(branchCode) + "," + salesMap.get(branchCode));
				bw.newLine();
			}
		}
	}
}
