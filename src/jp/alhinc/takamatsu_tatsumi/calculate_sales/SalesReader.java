package jp.alhinc.takamatsu_tatsumi.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class SalesReader {
	private String path;
	private Set<String> codes;

	SalesReader(String path, Set<String> codes) {
		this.path = path;
		this.codes = codes;
	}

	//調査後、売上ファイルから支店コードをもとに支店ごとの売り上げを計算してMap型に登録成功したらtrue
	public Map<String, Long> readSalesFile() throws IOException {

		Map<String, Long> salesMap = codes.stream().collect(Collectors.toMap(code -> code, code -> 0L));

		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File dir, String filename){
				return new File(dir, filename).isFile() && filename.matches("^\\d{8}(\\.rcd)$");
			}
		};

		List<File> rcdFiles = Arrays.asList(new File(path).listFiles(filter));
		TreeSet<File> numbers = new TreeSet<>(rcdFiles);

		if(numbers.isEmpty()) {
			return salesMap;
		}

		//連番になっているかを確認する処理を入れる。
		int min = Integer.parseInt(numbers.first().getName().substring(0, 8));
		int max = Integer.parseInt(numbers.last().getName().substring(0, 8));

		if(numbers.size() != (max - min +1)) {
			System.err.println("売上ファイル名が連番になっていません");
			return Collections.emptyMap();
		}

		for(File file : rcdFiles) {
			try (BufferedReader br = new BufferedReader(new FileReader(file));) {
				//計算するところ
				String code = br.readLine();
				String amount = br.readLine();

				if (br.readLine() != null) {
					System.err.println(file.getName() + "のフォーマットが不正です");
					return Collections.emptyMap();
				}

				if(!codes.contains(code)) {
					System.err.println(file.getName() + "の支店コードが不正です");
					return Collections.emptyMap();
				}

				Long salesMoney = Long.parseLong(amount);
				Long sum = salesMap.get(code) + salesMoney;
				//桁数が10桁より大きくなっていないかチェック
				if(sum.toString().length() > 10) {
					System.err.println("合計金額が10桁を超えました");
					return Collections.emptyMap();
				}
				salesMap.put(code, sum);
			}
		}
		return salesMap;
	}
}
