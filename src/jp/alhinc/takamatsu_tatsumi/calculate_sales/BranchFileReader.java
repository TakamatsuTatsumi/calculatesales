package jp.alhinc.takamatsu_tatsumi.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class BranchFileReader {
	private File path;

	BranchFileReader(String path) {
		this.path = new File(path, "branch.lst");
	}

	public Map<String, String> readBranchFile() throws IOException {
		Map<String, String> branchMap = new HashMap<>();

		if (!path.exists()) {
			System.err.println("支店定義ファイルが存在しません");
			return Collections.emptyMap();
		}

		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
			String line = "";
			while((line = br.readLine()) != null) {
				if(!(line.matches("\\d{3},[^,]+"))) {
					System.err.println("支店定義ファイルのフォーマットが不正です");
					return Collections.emptyMap();
				}
				String[] branchInfo = line.split(",");
				branchMap.put(branchInfo[0], branchInfo[1]);
			}
		}
		return branchMap;
	}
}
